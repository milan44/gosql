package gosql

import (
	"database/sql"
	"fmt"
	"strconv"
)

func _rowsToMap(rows sql.Rows) ([]map[string]string, error) {
	var result []map[string]string
	cols, _ := rows.Columns()

	for rows.Next() {
		data := make(map[string]string)
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i, _ := range columns {
			columnPointers[i] = &columns[i]
		}

		_ = rows.Scan(columnPointers...)

		for i, colName := range cols {
			data[colName] = getString(columns[i])
		}

		result = append(result, data)
	}

	if len(result) > 0 {
		return result, nil
	}
	return nil, nil
}

func getString(i interface{}) string {
	str, ok := i.(string)
	if !ok {
		in, ok := i.(int64)
		if !ok {
			fl, ok := i.(float64)
			if !ok {
				b, ok := i.(bool)
				if !ok {
					bt, ok := i.([]byte)
					if ok {
						return string(bt)
					}
				} else {
					if b {
						return "true"
					}
					return "false"
				}
			} else {
				return fmt.Sprint(fl)
			}
		} else {
			return strconv.FormatInt(in, 10)
		}
	}

	return str
}
