package gosql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"strconv"
	"strings"
)

type MySQLConnection struct {
	connection sql.DB
}

func NewConnection(username string, password string, hostname string, port int64, database string) (*MySQLConnection, error) {
	db, err := sql.Open("mysql", username+":"+password+"@tcp("+hostname+":"+strconv.FormatInt(port, 10)+")/"+database)
	if err != nil {
		return nil, err
	}

	conn := MySQLConnection{
		connection: *db,
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &conn, nil
}

func (m MySQLConnection) RawQuery(query string, getReturnValues bool) ([]map[string]string, error) {
	rows, err := m.connection.Query(query)
	if err != nil {
		return nil, err
	}

	if rows != nil && getReturnValues {
		return _rowsToMap(*rows)
	}

	err = rows.Close()
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (m MySQLConnection) PreparedQuery(query string, values []string, getReturnValues bool) ([]map[string]string, error) {
	s := make([]interface{}, len(values))
	for i, v := range values {
		s[i] = v
	}

	rows, err := m.connection.Query(query, s...)
	if err != nil {
		return nil, err
	}

	if rows != nil && getReturnValues {
		return _rowsToMap(*rows)
	}

	err = rows.Close()
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (m MySQLConnection) Create(table string, object map[string]string) error {
	s := "INSERT INTO `" + table + "` "

	var columns []string
	var questionMarks []string
	var values []string

	for key, value := range object {
		columns = append(columns, "`"+key+"`")
		values = append(values, value)
		questionMarks = append(questionMarks, "?")
	}

	s += "(" + strings.Join(columns, ", ") + ") VALUES (" + strings.Join(questionMarks, ", ") + ");"

	_, err := m.PreparedQuery(s, values, false)

	return err
}

func (m MySQLConnection) Read(table string, where string, values []string) ([]map[string]string, error) {
	s := "SELECT * FROM `" + table + "` WHERE " + where + ";"

	return m.PreparedQuery(s, values, true)
}

func (m MySQLConnection) Update(table string, where string, values []string, newObject map[string]string) ([]map[string]string, error) {
	s := "UPDATE `" + table + "` "

	var columns []string
	var newValues []string

	for key, value := range newObject {
		columns = append(columns, "`"+key+"` = ?")
		newValues = append(newValues, value)
	}

	for _, val := range values {
		newValues = append(newValues, val)
	}

	s += "SET " + strings.Join(columns, ", ") + " WHERE " + where + ";"

	return m.PreparedQuery(s, newValues, false)
}

func (m MySQLConnection) Delete(table string, where string, values []string) error {
	s := "DELETE FROM `" + table + "` WHERE " + where + ";"

	_, err := m.PreparedQuery(s, values, false)
	return err
}
func (m MySQLConnection) Destroy() error {
	return m.connection.Close()
}
