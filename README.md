# GoSQL

MySQL Library for golang.

### Create

```golang
m.Create("users", map[string]string{
    "username": "Steve",
    "password": "password123",
})
```

```mysql
INSERT INTO `users` (`username`, `password`)
VALUES ('Steve', 'password123');
```

### Read

```golang
data, err := m.Read("users", "`age` > ?", []string{"18"})
```

```mysql
SELECT * FROM `users` WHERE `age` > 18;
```

### Update

```golang
m.Update("users", "`username` = ?", []string{"Steve"}, map[string]string{
    "username": "Stephen",
})
```

```mysql
UPDATE `users` SET `username` = 'Stephen' WHERE `username` = 'Steve';
```

### Delete

```golang
m.Delete("users", "`username` = ?", []string{"Steve"})
```

```mysql
DELETE FROM `users` WHERE `username` = 'Steve';
```